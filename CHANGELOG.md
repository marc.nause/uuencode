# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://codeberg.org/marc.nause/uuencode/compare/2.0.0...HEAD)

### Added

- TBD

## [2.0.0](https://codeberg.org/marc.nause/uuencode/compare/1.0.2...2.0.0) - 2024-01-30

### Fixed

- Prevent finalizer attack by making UUEncoderStream final

## [1.0.2](https://codeberg.org/marc.nause/uuencode/compare/1.0.1...1.0.2) - 2020-02-11
### Added
- OSSRH clutter to pom.xml

## [1.0.1](https://codeberg.org/marc.nause/uuencode/compare/1.0.0...1.0.1) - 2020-02-10
### Added
- This changelog
- Code examples in the readme file
- Several unit tests

### Fixed
- Problems with data which contained Windows-style linebreaks

## [1.0.0](https://codeberg.org/marc.nause/uuencode/src/tag/1.0.0) - 2019-11-10
Initial release
