/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * Decorator classes for {@code java.io.OutputStream} which provide UUencode/UUdecode functionality.
 */
package de.audioattack.util.uunecode;
