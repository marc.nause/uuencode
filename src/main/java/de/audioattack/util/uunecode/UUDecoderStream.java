/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.util.uunecode;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Objects;

/**
 * This class implements an output stream for decoding data in the UUencode format.
 *
 * @since 1.0.0
 */
public class UUDecoderStream extends OutputStream {

    private static final char[] LINE_END_CHARS = "\n\r".toCharArray();

    private static final char[] MARKER_START = "begin".toCharArray();

    private static final String MARKER_END = "end";

    static {
        Arrays.sort(LINE_END_CHARS);
    }

    private byte length;

    private enum State {
        WAIT_FOR_HEADER, IN_BODY, END
    }

    private final LineBuffer lineBuffer = new LineBuffer(255);

    private final DecodingBuffer decodingBuffer = new DecodingBuffer();

    private final OutputStream outputStream;

    private Integer mode;

    private String filename;

    private byte lineLength;

    private State state = State.WAIT_FOR_HEADER;

    /**
     * Creates a new UU decoder output stream.
     *
     * @param outputStream the underlying output stream the decoded data is written to
     */
    public UUDecoderStream(@Nonnull final OutputStream outputStream) {
        this.outputStream = Objects.requireNonNull(outputStream, "outputStream must not br <null>");
    }

    /**
     * Writes the specified byte to this output stream.
     *
     * @param b the byte
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void write(final int b) throws IOException {

        final char c = (char) b;

        handleLineBuffer(c);

        switch (state) {
            case WAIT_FOR_HEADER:
            case END:
                lineBuffer.append(c);
                break;
            case IN_BODY:
                if (isLegalCharacter(c)) {
                    addDataToLineBuffer(c);
                }
                break;
            default:
                throw new IllegalStateException("Unknown state: " + state);
        }
    }

    private void handleLineBuffer(final char c) throws IOException {

        if (isEndCharacter(c) && state == State.WAIT_FOR_HEADER && lineBuffer.startsWith(MARKER_START)) {
            parseHeader();
        } else if (state == State.IN_BODY && lineBuffer.length() == lineLength) {
            decodeLine();
        } else if (state == State.END && lineBuffer.toString().trim().equals(MARKER_END)) {
            close();
        }
    }

    private void addDataToLineBuffer(final char c) {
        if (lineLength == 0) {
            length = AsciiConverter.toByte(c);
            lineLength = (byte) ((length / 3) * 4 + (length % 3 == 0 ? 0 : 4));
        } else {
            lineBuffer.append(c);
        }

        if (lineLength == 0) {
            state = State.END;
        }
    }

    private void decodeLine() throws IOException {
        for (int j = 0; j < lineBuffer.length(); j++) {
            if (decodingBuffer.add(AsciiConverter.toByte(lineBuffer.charAt(j)))) {
                length -= 3;
                writeBufferToOutput();
            }
        }
        lineLength = 0;
        lineBuffer.clear();
    }

    private void parseHeader() {
        final String[] h = lineBuffer.toString().split("\\s+");

        if (h.length == 3) {
            mode = Integer.parseInt(h[1].trim());
            filename = h[2].trim();
            state = State.IN_BODY;
            lineBuffer.clear();
        } else {
            throw new IllegalArgumentException("No valid header.");
        }
    }

    private void writeBufferToOutput() throws IOException {
        if (length < 0) {
            outputStream.write(decodingBuffer.getResult(), 0, 3 + length);
        } else {
            outputStream.write(decodingBuffer.getResult());
        }
    }

    /**
     * Flushes this output stream and forces any buffered output bytes to be written out.
     *
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public void flush() throws IOException {

        if (decodingBuffer.flush()) {
            outputStream.write(decodingBuffer.getResult());
        }
    }

    /**
     * Closes this output stream and releases any system resources associated with this stream.
     *
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void close() throws IOException {
        flush();
        outputStream.close();
        super.close();
    }

    /**
     * Gets the mode after decoding.
     *
     * @return the mode or {@code null} if data is not decoded yet
     */
    @Nullable
    public Integer getMode() {
        return mode;
    }

    /**
     * Gets the filename after decoding.
     *
     * @return the filename or {@code null} if data is not decoded yet
     */
    @Nullable
    public String getFilename() {
        return filename;
    }

    private static boolean isEndCharacter(final char c) {
        return Arrays.binarySearch(LINE_END_CHARS, c) >= 0;
    }

    private static boolean isLegalCharacter(final char c) {
        return c >= '!' && c <= '`';
    }
}
