/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.util.uunecode;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * This class implements an output stream for encoding data in the UUencode format.
 *
 * @since 1.0.0
 */
public final class UUEncoderStream extends OutputStream {

    private static final int LENGTH = 45;

    private static final Charset ASCII = StandardCharsets.US_ASCII;

    private static final byte[] NEWLINE = System.lineSeparator().getBytes(ASCII);

    private static final String HEADER = "begin %d %s" + System.lineSeparator();

    private static final byte[] END = ("`" + System.lineSeparator() + "end" + System.lineSeparator()).getBytes(ASCII);

    private static final int BUFFER_LENGTH = 60;

    private static final int DEFAULT_MODE = 644;

    private final EncodingBuffer encodingBuffer = new EncodingBuffer();

    private final OutputStream outputStream;

    private final char[] outputBuffer = new char[BUFFER_LENGTH];

    private int countEncBytes;

    private int countRawBytes;

    /**
     * Creates a new UU decoder output stream.
     * Begin line will be {@code begin 644 encoder.buf}
     *
     * @param outputStream the underlying output stream the encoded data is written to
     * @throws IOException if underlying output stream can not be written to
     * @since 1.1.0
     */
    public UUEncoderStream(@Nonnull final OutputStream outputStream) throws IOException {
        this(outputStream, "encoder.buf");
    }

    /**
     * Creates a new UU decoder output stream and specifies a name for the encoder buffer.
     * Begin line will be {@code begin 644 [FILENAME]}
     *
     * @param outputStream the underlying output stream the encoded data is written to
     * @param filename     the filename
     * @throws IOException if underlying output stream can not be written to
     * @since 1.1.0
     */
    public UUEncoderStream(@Nonnull final OutputStream outputStream, @Nonnull final String filename)
            throws IOException {
        this(outputStream, DEFAULT_MODE, filename);
    }

    /**
     * Creates a new UU decoder output stream and specifies mode and name for the encoder buffer.
     * Begin line will be {@code begin [MODE] [FILENAME]}
     *
     * @param outputStream the underlying output stream the encoded data is written to
     * @param mode         the file's Unix file permissions
     * @param filename     the filename
     * @throws IOException if underlying output stream can not be written to
     */
    public UUEncoderStream(@Nonnull final OutputStream outputStream, final int mode, @Nonnull final String filename)
            throws IOException {
        Objects.requireNonNull(filename, "filename must not be <null>");
        this.outputStream = Objects.requireNonNull(outputStream, "outputStream must not be <null>");

        outputStream.write(createHeader(mode, filename));
    }

    private byte[] createHeader(final int mode, final String filename) {

        return String.format(HEADER, mode, filename).getBytes(ASCII);
    }

    /**
     * Writes the specified byte to this output stream.
     *
     * @param b the byte
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void write(final int b) throws IOException {

        countRawBytes++;

        if (encodingBuffer.add((byte) b)) {

            copyEncodedBytesToOutputBuffer();

            if (countRawBytes == LENGTH) {
                write();
                countEncBytes = 0;
                countRawBytes = 0;
            }
        }
    }

    private void write() throws IOException {

        outputStream.write((byte) AsciiConverter.toChar((byte) countRawBytes));
        outputStream.write(new String(outputBuffer).getBytes(ASCII), 0, countEncBytes);
        outputStream.write(NEWLINE);
    }

    private void copyEncodedBytesToOutputBuffer() {

        for (final byte b : encodingBuffer.getResult()) {
            outputBuffer[countEncBytes++] = AsciiConverter.toChar(b);
        }
    }

    /**
     * Flushes this output stream and forces any buffered output bytes to be written out.
     *
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public void flush() throws IOException {
        if (encodingBuffer.flush()) {
            copyEncodedBytesToOutputBuffer();
            write();
        }

        countEncBytes = 0;
        countRawBytes = 0;

        writeFooter();
        outputStream.flush();
        super.flush();
    }

    /**
     * Closes this output stream and releases any system resources associated with this stream.
     *
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void close() throws IOException {
        flush();
        outputStream.close();
        super.close();
    }

    private void writeFooter() throws IOException {
        outputStream.write(END);
    }

}
