/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.util.uunecode;

import java.util.Arrays;

/* default */ class LineBuffer {

    private final char[] buffer;

    private int pointer = -1;

    LineBuffer(final int size) {
        buffer = new char[size];
    }

    public void append(final char c) {
        buffer[++pointer] = c;
    }

    public int length() {
        return pointer + 1;
    }

    public boolean isEmpty() {
        return pointer == -1;
    }

    public void clear() {
        pointer = -1;
    }

    public char charAt(final int i) {
        if (i > pointer) {
            throw new IndexOutOfBoundsException("Index must be lower than length: " + i + " " + length());
        }

        return buffer[i];
    }

    public boolean startsWith(final char[] chars) {
        return indexOf(chars) == 0;
    }

    public int indexOf(final char[] chars) {

        if (chars.length > length()) {
            return -1;
        }

        int result = -1;
        for (int i = 0; i < length(); i++) {

            if (buffer[i] == chars[0]) {

                for (int j = 0; j < chars.length; j++) {
                    if (buffer[i + j] != chars[j]) {
                        result = -1;
                        break;
                    }
                    result = i;
                }
                break;
            }
        }
        return result;
    }

    @Override
    public String toString() {

        return new String(Arrays.copyOfRange(buffer, 0, pointer + 1));
    }
}
