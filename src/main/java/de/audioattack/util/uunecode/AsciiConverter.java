/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.util.uunecode;

/* default */ final class AsciiConverter {

    private AsciiConverter() {
    }

    static char toChar(final byte b) {

        return b == 0 ? '`' : (char) (32 + b);
    }

    static byte toByte(final char c) {

        return c == '`' ? 0 : (byte) (c - 32);
    }

}
