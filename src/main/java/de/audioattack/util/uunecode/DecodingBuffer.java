/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.util.uunecode;

import java.util.Arrays;

/* default*/ class DecodingBuffer {

    /**
     * 4 byte bitmask.
     */
    public static final int BITMASK = 0b11111111;

    private final byte[] inBuffer = new byte[4];

    private final byte[] outBuffer = new byte[3];

    private int count;

    boolean add(final byte character) {

        inBuffer[count++] = character;
        if (count == inBuffer.length) {
            count = 0;
            decode();
        }

        return count == 0;
    }

    private void decode() {

        final int val = inBuffer[0] << 18 | inBuffer[1] << 12 | inBuffer[2] << 6 | inBuffer[3];

        outBuffer[0] = (byte) (val >> 16 & BITMASK);
        outBuffer[1] = (byte) (val >> 8 & BITMASK);
        outBuffer[2] = (byte) (val & BITMASK);
    }

    byte[] getResult() {

        return Arrays.copyOf(outBuffer, outBuffer.length);
    }

    boolean flush() {

        if (count == 0) {
            return false;
        } else {
            while (true) {
                if (add((byte) 0)) {
                    break;
                }
            }
            return true;
        }
    }
}
