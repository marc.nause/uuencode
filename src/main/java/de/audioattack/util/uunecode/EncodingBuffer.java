/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.util.uunecode;

import java.util.Arrays;

/* default*/ class EncodingBuffer {

    /**
     * 3 byte bitmask.
     */
    private static final int BITMASK = 0b00111111;

    private final byte[] inBuffer = new byte[3];

    private final byte[] outBuffer = new byte[4];

    private int count;

    boolean add(final byte b) {

        inBuffer[count++] = b;
        if (count == inBuffer.length) {
            count = 0;
            encode();
        }

        return count == 0;
    }

    private void encode() {

        final int val = inBuffer[0] << 16 | inBuffer[1] << 8 | inBuffer[2];

        outBuffer[0] = (byte) (val >> 18 & BITMASK);
        outBuffer[1] = (byte) (val >> 12 & BITMASK);
        outBuffer[2] = (byte) (val >> 6 & BITMASK);
        outBuffer[3] = (byte) (val & BITMASK);
    }

    byte[] getResult() {

        return Arrays.copyOf(outBuffer, outBuffer.length);
    }

    boolean flush() {

        if (count == 0) {
            return false;
        } else {
            while (true) {
                if (add((byte) 0)) {
                    break;
                }
            }
            return true;
        }
    }
}
