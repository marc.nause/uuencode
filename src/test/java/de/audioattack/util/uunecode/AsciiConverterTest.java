package de.audioattack.util.uunecode;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AsciiConverterTest {

    private static final char[] CHARS =
            "`!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_".toCharArray();

    @Test
    void testToChar() {

        for (byte i = 0; i < 64; i++) {
            assertEquals(CHARS[i], AsciiConverter.toChar(i));
        }
    }

    @Test
    void testToByte() {

        for (int i = 0; i < 64; i++) {
            assertEquals(i, AsciiConverter.toByte(CHARS[i]));
        }
    }

}
