package de.audioattack.util.uunecode;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

class UUEncoderStreamTest {

    @Test
    void test() throws IOException {

        final byte[] buffer = new byte[128];
        int numBytes;

        final ByteArrayOutputStream expected = new ByteArrayOutputStream();
        final InputStream expIS = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_uuencoded.txt");
        while ((numBytes = expIS.read(buffer)) > 0) {
            expected.write(buffer, 0, numBytes);
        }

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final InputStream raw = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_plaintext.txt");
        final UUEncoderStream uuEncoderStream = new UUEncoderStream(baos, 644, "valid_plaintext.txt");
        while ((numBytes = raw.read(buffer)) > 0) {
            uuEncoderStream.write(buffer, 0, numBytes);
        }
        uuEncoderStream.flush();

        assertArrayEquals(expected.toByteArray(), baos.toByteArray());

    }

    @Test
    void testDefaultConstructor() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final UUEncoderStream uuEncoderStream = new UUEncoderStream(baos);
        uuEncoderStream.close();

        assertTrue(baos.toString().startsWith("begin 644 encoder.buf"));
    }

    @Test
    void testConstructorWithName() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final UUEncoderStream uuEncoderStream = new UUEncoderStream(baos, "text.txt");
        uuEncoderStream.close();

        assertTrue(baos.toString().startsWith("begin 644 text.txt"));
    }

    @Test
    void testConstructorWithNameAndMode() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final UUEncoderStream uuEncoderStream = new UUEncoderStream(baos, 777, "text.bin");
        uuEncoderStream.close();

        assertTrue(baos.toString().startsWith("begin 777 text.bin"));
    }

    @Test
    void testConstructorWithOutputStreamNull() {
        assertThrows(NullPointerException.class, () -> new UUEncoderStream(null));
        assertThrows(NullPointerException.class, () -> new UUEncoderStream(null, "xyz.abc"));
        assertThrows(NullPointerException.class, () -> new UUEncoderStream(null, 123, "xyz.abc"));
    }

    @Test
    void testConstructorWithFilenameNull() {
        assertThrows(NullPointerException.class, () -> new UUEncoderStream(new ByteArrayOutputStream(), null));
        assertThrows(NullPointerException.class, () -> new UUEncoderStream(new ByteArrayOutputStream(), 123, null));
    }

    @Test
    void testConstructorWithAllNull() {
        assertThrows(NullPointerException.class, () -> new UUEncoderStream(null, 123, null));
    }
}
