package de.audioattack.util.uunecode;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DecodingBufferTest {

    @Test
    void testDecodeResult() {

        final DecodingBuffer decBuff = new DecodingBuffer();
        assertFalse(decBuff.add((byte) 1));
        assertFalse(decBuff.add((byte) 2));
        assertFalse(decBuff.add((byte) 3));
        assertTrue(decBuff.add((byte) 4));
    }

    @Test
    void testDecoding1() {

        final DecodingBuffer decBuff = new DecodingBuffer();
        decBuff.add((byte) 24);
        decBuff.add((byte) 16);
        decBuff.add((byte) 0);
        decBuff.add((byte) 0);
        decBuff.flush();

        assertArrayEquals(new byte[]{(byte) 'a', (byte) 0, (byte) 0}, decBuff.getResult());
    }

    @Test
    void testDecoding2() {

        final DecodingBuffer decBuff = new DecodingBuffer();
        decBuff.add((byte) 24);
        decBuff.add((byte) 22);
        decBuff.add((byte) 8);
        decBuff.add((byte) 0);
        decBuff.flush();

        assertArrayEquals(new byte[]{(byte) 'a', (byte) 'b', (byte) 0}, decBuff.getResult());
    }

    @Test
    void testDecoding3() {
        // example taken from https://en.wikipedia.org/wiki/Uuencoding#Formatting_mechanism

        final DecodingBuffer decBuff = new DecodingBuffer();
        decBuff.add((byte) 16);
        decBuff.add((byte) 54);
        decBuff.add((byte) 5);
        decBuff.add((byte) 52);

        assertArrayEquals(new byte[]{(byte) 'C', (byte) 'a', (byte) 't'}, decBuff.getResult());
    }

    @Test
    void testDecoding4() {

        final DecodingBuffer decBuff = new DecodingBuffer();
        decBuff.add((byte) 16);
        decBuff.add((byte) 54);
        decBuff.add((byte) 5);
        decBuff.add((byte) 52);
        decBuff.add((byte) 29);
        decBuff.add((byte) 16);
        decBuff.flush();

        assertArrayEquals(new byte[]{(byte) 'u', (byte) 0, (byte) 0}, decBuff.getResult());
    }

    @Test
    void testFlush() {

        final DecodingBuffer decBuff = new DecodingBuffer();
        decBuff.add((byte) 1);
        decBuff.flush();

        assertArrayEquals(new byte[]{4, 0, 0}, decBuff.getResult());
    }

}
