package de.audioattack.util.uunecode;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LineBufferTest {

    @Test
    void testLength() {

        final LineBuffer buffer = new LineBuffer(255);
        assertEquals(0, buffer.length());
        for (int i = 1; i <= 255; i++) {
            buffer.append((char) i);
            assertEquals(i, buffer.length());
        }
    }

    @Test
    void testCharAt() {

        final LineBuffer buffer = new LineBuffer(255);
        assertEquals(0, buffer.length());
        for (int i = 0; i < 255; i++) {
            buffer.append((char) i);
            assertEquals(i, buffer.charAt(i));
        }

        for (int i = 0; i < 255; i++) {
            assertEquals(i, buffer.charAt(i));
        }
    }

    @Test
    void testCharAtOOB() {

        final LineBuffer buffer = new LineBuffer(255);
        assertThrows(IndexOutOfBoundsException.class, () -> buffer.charAt(0));
    }

    @Test
    void testCharAtNegativeOOB() {

        final LineBuffer buffer = new LineBuffer(255);
        assertThrows(IndexOutOfBoundsException.class, () -> buffer.charAt(-1));
    }

    @Test
    void testCharAtOOBAfterClear() {

        final LineBuffer buffer = new LineBuffer(255);
        buffer.append('x');
        assertEquals('x', buffer.charAt(0));
        buffer.clear();
        assertThrows(IndexOutOfBoundsException.class, () -> buffer.charAt(0));
    }

    @Test
    void testClear() {

        final LineBuffer buffer = new LineBuffer(255);
        assertEquals(0, buffer.length());
        for (int i = 1; i <= 255; i++) {
            buffer.append((char) i);
        }
        assertEquals(255, buffer.length());
        buffer.clear();
        assertEquals(0, buffer.length());
    }

    @Test
    void testToString() {

        final LineBuffer buffer = new LineBuffer(255);
        assertEquals("", buffer.toString());
        buffer.append('a');
        assertEquals("a", buffer.toString());
        buffer.append('b');
        assertEquals("ab", buffer.toString());
        buffer.append('c');
        assertEquals("abc", buffer.toString());
    }

    @Test
    void testToIndexOf() {

        final LineBuffer buffer = new LineBuffer(255);

        buffer.append('a');
        buffer.append('b');
        buffer.append('c');
        buffer.append('d');
        buffer.append('c');

        assertEquals(0, buffer.indexOf("ab".toCharArray()));
        assertEquals(1, buffer.indexOf("bc".toCharArray()));
        assertEquals(2, buffer.indexOf("cd".toCharArray()));
        assertEquals(3, buffer.indexOf("dc".toCharArray()));
        assertEquals(-1, buffer.indexOf("ac".toCharArray()));
        assertEquals(-1, buffer.indexOf("abcdea".toCharArray()));

        assertEquals(-1, buffer.indexOf("x".toCharArray()));
    }

    @Test
    void testStartsWith() {

        final LineBuffer buffer = new LineBuffer(255);
        buffer.append('a');
        buffer.append('b');
        buffer.append('c');
        buffer.append('d');
        buffer.append('c');

        assertTrue(buffer.startsWith("a".toCharArray()));
        assertTrue(buffer.startsWith("ab".toCharArray()));
        assertTrue(buffer.startsWith("abc".toCharArray()));
        assertTrue(buffer.startsWith("abcd".toCharArray()));
        assertTrue(buffer.startsWith("abcdc".toCharArray()));

        assertFalse(buffer.startsWith("b".toCharArray()));
        assertFalse(buffer.startsWith("c".toCharArray()));
        assertFalse(buffer.startsWith("d".toCharArray()));
    }

    @Test
    void testIsEmpty() {
        final LineBuffer buffer = new LineBuffer(16);
        assertTrue(buffer.isEmpty());
        buffer.append('!');
        assertFalse(buffer.isEmpty());
    }

}
