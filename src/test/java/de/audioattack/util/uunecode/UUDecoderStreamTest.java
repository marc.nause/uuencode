package de.audioattack.util.uunecode;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UUDecoderStreamTest {

    @Test
    void test() throws IOException {

        final byte[] buffer = new byte[128];
        int numBytes;

        final ByteArrayOutputStream expected = new ByteArrayOutputStream();
        final InputStream expIS = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_plaintext.txt");
        while ((numBytes = expIS.read(buffer)) > 0) {
            expected.write(buffer, 0, numBytes);
        }

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final InputStream raw = Thread.currentThread().getContextClassLoader().getResourceAsStream("valid_uuencoded.txt");
        final UUDecoderStream uuDecoderStream = new UUDecoderStream(baos);
        while ((numBytes = raw.read(buffer)) > 0) {
            uuDecoderStream.write(buffer, 0, numBytes);
        }

        assertEquals(Integer.valueOf(644), uuDecoderStream.getMode());
        assertEquals("valid_plaintext.txt", uuDecoderStream.getFilename());

        assertArrayEquals(expected.toByteArray(), baos.toByteArray());
    }

    @Test
    void testWindowsLineEndings() throws IOException {
        final String uuencoded = "begin 0744 test.txt\r\n" +
                "M5&AE('%U:6-K(&)R;W=N(&9O>\"!J=6UP<R!O=F5R('1H92!L87IY(&1O9RX@\r\n" +
                "M5&AE('%U:6-K(&)R;W=N(&9O>\"!J=6UP<R!O=F5R('1H92!L87IY(&1O9RX@\r\n" +
                "M5&AE('%U:6-K(&)R;W=N(&9O>\"!J=6UP<R!O=F5R('1H92!L87IY(&1O9RX@\r\n" +
                "L5&AE('%U:6-K(&)R;W=N(&9O>\"!J=6UP<R!O=F5R('1H92!L87IY(&1O9RX`\r\n" +
                "`\r\n" +
                "end\r\n";

        final String decoded = "The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy " +
                "dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.";

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final UUDecoderStream uuDecoderStream = new UUDecoderStream(baos);
        uuDecoderStream.write(uuencoded.getBytes(StandardCharsets.US_ASCII));

        assertEquals(decoded, baos.toString());
        assertEquals(Integer.valueOf(744), uuDecoderStream.getMode());
        assertEquals("test.txt", uuDecoderStream.getFilename());
    }

    @Test
    void testTooManyZeroLengthLines() throws IOException {
        final String uuencoded = "begin 0744 test.txt\r\n" +
                "M5&AE('%U:6-K(&)R;W=N(&9O>\"!J=6UP<R!O=F5R('1H92!L87IY(&1O9RX@\n" +
                "M5&AE('%U:6-K(&)R;W=N(&9O>\"!J=6UP<R!O=F5R('1H92!L87IY(&1O9RX@\n" +
                "M5&AE('%U:6-K(&)R;W=N(&9O>\"!J=6UP<R!O=F5R('1H92!L87IY(&1O9RX@\n" +
                "L5&AE('%U:6-K(&)R;W=N(&9O>\"!J=6UP<R!O=F5R('1H92!L87IY(&1O9RX`\n" +
                "`\n" +
                "`\n" +
                "`\n" +
                "`\n" +
                "end";

        final String decoded = "The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy " +
                "dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.";

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final UUDecoderStream uuDecoderStream = new UUDecoderStream(baos);
        uuDecoderStream.write(uuencoded.getBytes(StandardCharsets.US_ASCII));

        assertEquals(decoded, baos.toString());
        assertEquals(Integer.valueOf(744), uuDecoderStream.getMode());
        assertEquals("test.txt", uuDecoderStream.getFilename());
    }

    @Test
    void testConstructorWithNull() {
        assertThrows(NullPointerException.class, () -> new UUDecoderStream(null));
    }

    @Test
    void testInvalidHeader() {

        final UUDecoderStream uuDecoderStream = new UUDecoderStream(new ByteArrayOutputStream());
        final byte[] bytes = "begin\r".getBytes(StandardCharsets.US_ASCII);
        assertThrows(IllegalArgumentException.class, () -> uuDecoderStream.write(bytes));
    }

}
