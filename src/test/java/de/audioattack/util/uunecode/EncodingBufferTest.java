package de.audioattack.util.uunecode;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EncodingBufferTest {

    @Test
    void testEncodeResult() {
        final EncodingBuffer encBuff = new EncodingBuffer();
        assertFalse(encBuff.add((byte) 1));
        assertFalse(encBuff.add((byte) 2));
        assertTrue(encBuff.add((byte) 3));
    }

    @Test
    void testEncoding1() {
        // example taken from https://en.wikipedia.org/wiki/Uuencoding#Formatting_mechanism

        final EncodingBuffer encBuff = new EncodingBuffer();
        encBuff.add((byte) 'a');
        encBuff.flush();

        assertArrayEquals(new byte[]{24, 16, 0, 0}, encBuff.getResult());
    }

    @Test
    void testEncoding2() {
        // example taken from https://en.wikipedia.org/wiki/Uuencoding#Formatting_mechanism

        final EncodingBuffer encBuff = new EncodingBuffer();
        encBuff.add((byte) 'a');
        encBuff.add((byte) 'b');
        encBuff.flush();

        assertArrayEquals(new byte[]{24, 22, 8, 0}, encBuff.getResult());
    }

    @Test
    void testEncoding3() {
        // example taken from https://en.wikipedia.org/wiki/Uuencoding#Formatting_mechanism

        final EncodingBuffer encBuff = new EncodingBuffer();
        encBuff.add((byte) 'C');
        encBuff.add((byte) 'a');
        encBuff.add((byte) 't');
        encBuff.flush();

        assertArrayEquals(new byte[]{16, 54, 5, 52}, encBuff.getResult());
    }

    @Test
    void testEncoding4() {

        final EncodingBuffer encBuff = new EncodingBuffer();
        encBuff.add((byte) 'C');
        encBuff.add((byte) 'a');
        encBuff.add((byte) 't');
        encBuff.add((byte) 'u');
        encBuff.flush();

        assertArrayEquals(new byte[]{29, 16, 0, 0}, encBuff.getResult());
    }

    @Test
    void testFlushEmpty() {

        final EncodingBuffer encBuff = new EncodingBuffer();
        assertFalse(encBuff.flush());
    }

    @Test
    void testFlushNotEmpty() {

        final EncodingBuffer encBuff = new EncodingBuffer();
        encBuff.add((byte) 23);
        assertTrue(encBuff.flush());
    }

    @Test
    void testFlushResult() {

        final EncodingBuffer encBuff = new EncodingBuffer();
        encBuff.add((byte) 4);
        encBuff.flush();

        assertArrayEquals(new byte[]{1, 0, 0, 0}, encBuff.getResult());
    }

}
