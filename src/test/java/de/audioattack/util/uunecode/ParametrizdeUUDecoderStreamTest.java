package de.audioattack.util.uunecode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class ParametrizdeUUDecoderStreamTest {

    static Stream<Arguments> data() {
        return Stream.of(
                arguments("!80``\n", "a"),
                arguments("\"86$`\n", "aa"),
                arguments("#86%A\n", "aaa"),
                arguments("$86%A80``\n", "aaaa"),
                arguments("%86%A86$`\n", "aaaaa"),
                arguments("&86%A86%A\n", "aaaaaa"),
                arguments("'86%A86%A80``\n", "aaaaaaa"),
                arguments("(86%A86%A86$`\n", "aaaaaaaa"),
                arguments(")86%A86%A86%A\n", "aaaaaaaaa"),
                arguments("*86%A86%A86%A80``\n", "aaaaaaaaaa"),
                arguments("+86%A86%A86%A86$`\n", "aaaaaaaaaaa"),
                arguments(",86%A86%A86%A86%A\n", "aaaaaaaaaaaa"),
                arguments("-86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaa"),
                arguments(".86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaa"),
                arguments("/86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaa"),
                arguments("086%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaa"),
                arguments("186%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaa"),
                arguments("286%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaa"),
                arguments("386%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaa"),
                arguments("486%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaa"),
                arguments("586%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaa"),
                arguments("786%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("886%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("986%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments(":86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments(";86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("<86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("=86%A86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments(">86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("?86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("@86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("B86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("C86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("D86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("E86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("F86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("G86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("H86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("I86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("J86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("K86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("L86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("M86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("M86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n!80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("M86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n\"86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("M86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n#86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("M86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n$86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("N86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("O86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("P86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("Q86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("R86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("S86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("T86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("U86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86$`\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("V86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("W86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("X86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86$\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("Y86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"),
                arguments("Z86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A86%A80``\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        );
    }

    @ParameterizedTest
    @MethodSource("data")
    void test(final String encoded, final String decoded) throws IOException {

        final String uuencoded = "begin 0744 test.txt\r\n" +
                encoded +
                "`\n" +
                "end";

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final UUDecoderStream uuDecoderStream = new UUDecoderStream(baos);
        uuDecoderStream.write(uuencoded.getBytes(StandardCharsets.US_ASCII));

        assertEquals(decoded, baos.toString());
        assertEquals(Integer.valueOf(744), uuDecoderStream.getMode());
        assertEquals("test.txt", uuDecoderStream.getFilename());
    }
}
