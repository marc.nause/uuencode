# uuencode
This software provides simple [UUencode](https://de.wikipedia.org/wiki/Uuencode)/UUdecode functionality. The library is written in Java.

# Usage
The software uses the [decorator design pattern](https://sourcemaking.com/design_patterns/decorator) to provide UUencode/UUdecode functionality. It fits the philosophy of the [Java IO package](https://docs.oracle.com/javase/8/docs/api/java/io/package-summary.html), hence most developers should have no problems using the library.


The following code writes UUencoded data:
```
UUEncoderStream uuEncoderStream = new UUEncoderStream(outputStream, 644, "valid_plaintext.txt");
uuEncoderStream.write(data);
uuEncoderStream.close();
```
It is assumed that ```outputStream``` is a valid ```OutputStream``` (e.g. a ```FileOutputStream```) the encoded data is written to and ```data``` contains the data to be encoded.

The code decoding UUencoded data looks similar:
```
 UUDecoderStream uuDecoderStream = new UUDecoderStream(outputStream);
 uuDecoderStream.write(data.getBytes(StandardCharsets.US_ASCII));
 uuDecoderStream.close();
 int mode = uuDecoderStream.getMode();
 String filename = uuDecoderStream.getFilename();
```
It is assumed that ```outputStream``` is a valid ```OutputStream``` (e.g. a ```FileOutputStream```) the decoded data is
written to and ```data``` contains the encoded data that is to be decoded. After decoding ```mode``` and ```filename```
from the header line of the encoded data, the values can be retrieved from the ```UUDecoderStream``` instance via the
respective getters.

# Releases

The latest release is available
on [Maven Central Repository](https://central.sonatype.com/artifact/de.audioattack.util/uuencode).

Maven dependency:

```
<dependency>
  <groupId>de.audioattack.util</groupId>
  <artifactId>uuencode</artifactId>
  <version>2.0.0</version>
</dependency>
```

Gradle dependency:

```
implementation("de.audioattack.util:uuencode:2.0.0")
```

# Documentation
The API documentation can be viewed online on [javadoc.io](https://www.javadoc.io/doc/de.audioattack.util/uuencode/latest/).

# License
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
